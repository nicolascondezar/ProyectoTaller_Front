import React, { useState,useEffect } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import axios from 'axios'
import MaterialDatatable from "material-datatable";
import Swal from 'sweetalert2'

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export default function Persona() {
    const [nombre, setNombre] = useState("");
    const [apellido_paterno, setApellido_paterno] = useState("");
    const [apellido_materno, setApellido_materno] = useState("");
    const [autor, setAutor] = useState("");
    const [titulo, setTitulo] = useState("");
    const [ano, setAno] = useState("");
    const [data, setData] = useState([]);
    const [data2, setData2] = useState([]);
    const [idpersona, setIDPersona] = useState("");
    const [idlibro, setIDLibro] = useState("");
    const [fecha, setFecha] = useState("");
    const [accion,setAccion] = useState("Guardar")

    useEffect(() => {

        Listar();
      },[]);



    const classes = useStyles();


    const columns = [

        {
            name: "Sel",
            options: {
              headerNoWrap: true,
              customBodyRender: (item) => {
                return (
                  <Button
                    variant="contained"
                    className="btn-block"
                    onClick={() =>{
                        setIDPersona(item.idpersona)
                        setIDLibro(item.idlibro)
                        setFecha(item.fecha)
                    }}
                  >
                    Seleccionar
                  </Button>
                );
              },
            },
          },
        {
            name: 'Nombre',
            field: 'nombre',
        },
        {
            name: 'Apellido Paterno',
            field: 'apellido_paterno',
        },
        {
            name: 'Apellido Materno',
            field: 'apellido_materno',
        },
        {
            name: 'Titulo',
            field: 'titulo',
        },

    ];

    const options = {
        selectableRows:false
    };



    const Listar = () =>{

        axios
            .get(
                `http://localhost:8081/api/prestamo`
            )
            .then(
                (response) => {
                    setData(response.data)
             
                },
                (error) => {
             
                }



            );
    }

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Registro Persona
                </Typography>
                <form className={classes.form} noValidate>
                    <Grid container justify="flex-end">
                        <MaterialDatatable
                            title={"lista de prestamos"}
                            data={data}
                            columns={columns}
                            options={options}
                        />


                    </Grid>
                </form>
            </div>

        </Container>
    );
}