import React, { useState,useEffect } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import axios from 'axios'
import MaterialDatatable from "material-datatable";
import Swal from 'sweetalert2'

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export default function Libro() {
    const [autor, setAutor] = useState("");
    const [titulo, setTitulo] = useState("");
    const [ano, setAno] = useState("");
    const [data, setData] = useState([]);
    const [accion,setAccion] = useState("Guardar")

    useEffect(() => {

        Listar();
      },[]);



    const classes = useStyles();


    const columns = [

        {
            name: "Sel",
            options: {
              headerNoWrap: true,
              customBodyRender: (item) => {
                return (
                  <Button
                    variant="contained"
                    className="btn-block"
                    onClick={() =>{
                        setAutor(item.autor)
                        setTitulo(item.titulo)
                        setAno(item.ano)
                    }}
                  >
                    Seleccionar
                  </Button>
                );
              },
            },
          },
        {
            name: 'Autor',
            field: 'autor',
        },
        {
            name: 'Titulo',
            field: 'titulo',
        },
        {
            name: 'Año',
            field: 'ano',
        },

    ];

    const options = {
        selectableRows:false
    };



    const Listar = () =>{

        axios
            .get(
                `http://localhost:8081/api/libro`
            )
            .then(
                (response) => {
                    setData(response.data)
             
                },
                (error) => {
             
                }



            );
    }

    const Guardar = () => {


        // alert(nombre);
        // alert(apellido);
        // alert(rut);

      if(accion=="Guardar"){
        axios
        .post(
            `http://localhost:8081/api/libro`, {
            autor: autor,
            titulo: titulo,
            ano: ano
        }
        )
        .then(
            (response) => {
                if (response.status == 201) {
                    //alert("Registro Correcto")
                    Swal.fire({
                        title: 'Perfecto!',
                        text: 'Registro Correcto',
                        icon: 'success',
                        confirmButtonText: 'ok'
                      })
                    Listar();
                    Limpiar();
                }

            },
            (error) => {

                alert("Error al registrar")
            }



        );
      }  
        

      
    } 
    const Limpiar = () =>{
        setAutor("");
        setTitulo("");
        setAno("");
        setAccion("Guardar");
    }

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Registro Libro
                </Typography>
                <form className={classes.form} noValidate>
                    <Grid container spacing={2}>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                value={autor}
                                onChange={(evt) => {

                                    setAutor(evt.target.value)
                                }}
                                autoComplete="NAutor"
                                name="NombreAutor"
                                variant="outlined"
                                required
                                fullWidth
                                id="NAutor"
                                label="Autor"
                                autoFocus
                            />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                value={titulo}
                                onChange={(evt) => {

                                    setTitulo(evt.target.value)
                                }}
                                variant="outlined"
                                required
                                fullWidth
                                id="LTitle"
                                label="Titulo"
                                name="Ltitulo"
                                autoComplete="ltitle"
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                value={ano}
                                onChange={(evt) => {

                                    setAno(evt.target.value)
                                }}
                                variant="outlined"
                                required
                                fullWidth
                                id="ano"
                                label="año"
                                name="ano"
                                autoComplete="ano"
                            />
                        </Grid>

                    </Grid>
                    <Button
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        onClick={() => Guardar()}
                    >
                        {accion}
                    </Button>
                    <Grid container justify="flex-end">
                        <MaterialDatatable
                            title={"Lista de Libros"}
                            data={data}
                            columns={columns}
                            options={options}
                        />


                    </Grid>
                </form>
            </div>

        </Container>
    );
}